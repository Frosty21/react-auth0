import React, { useEffect } from "react";

const Callback = (props) => {
  const location = props.location;
  const auth = props.auth;
  useEffect(() => {
    if (/access_token|id_token|error/.test(location.hash)) {
      auth.handleAuthentication();
    } else {
      throw new Error("Invalid callback URL.");
    }
  }, [location, auth]);
  return <h1>Loading...</h1>;
};
export default Callback;
