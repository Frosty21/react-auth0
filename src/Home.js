import React, { Component } from "react";
import { Link } from "react-router-dom";
class Home extends Component {
  render() {
    const { isAuthenticated, login } = this.props.auth;
    return (
      <div>
        <h1>Home</h1>
        {isAuthenticated() ? (
          <Link to="/profile"> View Profile </Link>
        ) : (
          <button onClick={login}>Login</button>
        )}
      </div>
    );
  }
}

export default Home;

// using hooks
// import React, { useState, useEffect } from "react";

// function Home() {
//   return (
//     <div>
//       <h1>Home</h1>
//     </div>
//   );
// }

// export default Home;
