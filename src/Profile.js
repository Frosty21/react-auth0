import React, { useState, useEffect } from "react";

function Profile(props) {
  const [profile, setProfile] = useState({ profile: null, error: "" });

  useEffect(() => {
    loadUserProfile();
  });

  const loadUserProfile = () => {
    return props.auth.getProfile((profile, error) =>
      setProfile(profile, error)
    );
  };
  if (!profile) return null;
  return (
    <>
      <h1>Profile</h1>
      <p>{profile.nickname}</p>
      <img
        style={{ maxWidth: 50, maxHeight: 50 }}
        src={profile.picture}
        alt="profile pic"
      />
      <pre>{JSON.stringify(profile, null, 2)}</pre>
    </>
  );
}
export default Profile;
